# Notifications Source

This repository serves as a "master" repository for development and translation of notifications for Adblock Plus before publishing them via the "real" notification repository <https://hg.adblockplus.org/notifications>.
